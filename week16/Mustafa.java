import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.Timestamp;
import java.sql.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

public class Mustafa {

	static LinkedList<String> object;
	static Set<String> hash_Set;
	public static void main(String[] args) throws IOException {
		
		
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("enter the linkedlist file name :");
		String file_name_1 = scanner.nextLine();
		System.out.print("enter the set file name :");
		String file_name_2 = scanner.nextLine();
		
		long startTime = System.nanoTime(); 
        
	    
		 LinkedList<String> object = new LinkedList<String>();
		 Set<String> hash_Set = new HashSet<String>();
		 LinkedList<String> sil = new LinkedList<String>();
		 
		 File(object,hash_Set,file_name_1,1);
		 File(object,hash_Set,file_name_2,2);
		 
		 for(int i=0; i< object.size(); i++)
		    {
		      
			   String first=object.get(i);			 
			   if(hash_Set.contains(first)){
					sil.add(first);
				 }
		    }
		 
		 for(int i=0; i< sil.size(); i++)
		    {
		    
			 String first=sil.get(i);
			 object.remove(first);
			 
		    }
		 System.out.println("different strings:  "+object);
		 
		   long estimatedTime = System.nanoTime() - startTime;
		    System.out.println("process takes :"+estimatedTime+ " nanosec");
		     
	}
 
	
	 
	public static void File(LinkedList<String> object,Set<String> hash_Set,String fileName,int file) throws IOException{
		   
		try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(fileName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line=null;
			while((line = bufferedReader.readLine()) != null) {
                   
                   
                   if(file==2){
                	    hash_Set.add(line);
                   }
                   else{
                	   object.add(line);
                   }
              
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
	}
}
